<?php

/**
 * Magento sitemap custom urls
 *
 * @category   LCB
 * @package    LCB_Sitemap
 * @author     Silpion Tomasz Gregorczyk <tomasz@silpion.com.pl>
 */
class LCB_Sitemap_Model_Sitemap_Sitemap extends Mage_Sitemap_Model_Sitemap {

    protected $io;
    public $type;
    public $priority;
    public $baseUrl;

    public function getSitemapType()
    {
        return "lcb";
    }

    public function generateXml()
    {

        $storeId = $this->getStoreId();
        $this->baseUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

        $date = Mage::getSingleton('core/date')->gmtDate('Y-m-d');

        if ($this->getSitemapType() != 'lcb') {
            return parent::generateXml();
        }

        $this->io = new Varien_Io_File();
        $this->io->setAllowCreateFolders(true);
        $this->io->open(array('path' => $this->getPath()));

        if ($this->io->fileExists($this->getSitemapFilename()) && !$this->io->isWriteable($this->getSitemapFilename())) {
            Mage::throwException(Mage::helper('lcb_sitemap')->__('File "%s" cannot be saved. Please, make sure the directory "%s" is writeable by web server.', $this->getSitemapFilename(), $this->getPath()));
        }

        $this->io->streamOpen($this->getSitemapFilename());

        $this->io->streamWrite('<?xml version="1.0" encoding="UTF-8"?>' . "\n");
        $this->io->streamWrite('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">');

        /**
         * Generate categories sitemap
         */
        $changefreq = (string) Mage::getStoreConfig('sitemap/category/changefreq', $storeId);
        $priority = (string) Mage::getStoreConfig('sitemap/category/priority', $storeId);
        $collection = Mage::getResourceModel('sitemap/catalog_category')->getCollection($storeId);
        $categories = new Varien_Object();
        $categories->setItems($collection);
        Mage::dispatchEvent('sitemap_categories_generating_before', array(
            'collection' => $categories
        ));
        foreach ($categories->getItems() as $item) {
            $xml = sprintf(
                    '<url><loc>%s</loc><lastmod>%s</lastmod><changefreq>%s</changefreq><priority>%.1f</priority></url>', htmlspecialchars($this->baseUrl . $item->getUrl()), $date, $changefreq, $priority
            );
            $this->io->streamWrite($xml);
        }
        unset($collection);

        /**
         * Generate products sitemap
         */
        $changefreq = (string) Mage::getStoreConfig('sitemap/product/changefreq', $storeId);
        $priority = (string) Mage::getStoreConfig('sitemap/product/priority', $storeId);
        $collection = Mage::getResourceModel('sitemap/catalog_product')->getCollection($storeId);
        $products = new Varien_Object();
        $products->setItems($collection);
        Mage::dispatchEvent('sitemap_products_generating_before', array(
            'collection' => $products
        ));
        foreach ($products->getItems() as $item) {
            $xml = sprintf(
                    '<url><loc>%s</loc><lastmod>%s</lastmod><changefreq>%s</changefreq><priority>%.1f</priority></url>', htmlspecialchars($this->baseUrl . $item->getUrl()), $date, $changefreq, $priority
            );
            $this->io->streamWrite($xml);
        }
        unset($collection);

        /**
         * Generate cms pages sitemap
         */
        $changefreq = (string) Mage::getStoreConfig('sitemap/page/changefreq', $storeId);
        $priority = (string) Mage::getStoreConfig('sitemap/page/priority', $storeId);
        $collection = Mage::getResourceModel('sitemap/cms_page')->getCollection($storeId);
        foreach ($collection as $item) {
            $xml = sprintf(
                    '<url><loc>%s</loc><lastmod>%s</lastmod><changefreq>%s</changefreq><priority>%.1f</priority></url>', htmlspecialchars($this->baseUrl . $item->getUrl()), $date, $changefreq, $priority
            );
            $this->io->streamWrite($xml);
        }
        unset($collection);

        /**
         * Generate custom links sitemap
         */
        $collection = Mage::getModel('lcb_sitemap/links')->getCollection();
        $this->priority = '0.5';
        $this->type = 'custom';
        $this->generateSitemap($collection);
        unset($collection);

        $this->io->streamWrite('</urlset>');
        $this->io->streamClose();

        $this->setSitemapTime(Mage::getSingleton('core/date')->gmtDate('Y-m-d H:i:s'));
        $this->save();

        return $this;
    }

    public function generateSitemap($collection)
    {
        $date = Mage::getSingleton('core/date')->gmtDate('Y-m-d');
        $changefreq = 'weekly';
        $priority = $this->priority;
        foreach ($collection as $item) {
            switch ($this->type) {
                case 'product':
                    $url = $this->baseUrl . $item->getUrlKey();
                    break;
                default:
                    $url = $this->baseUrl . $item->getUrl();
                    break;
            }

            $xml = sprintf('<url><loc>%s</loc><lastmod>%s</lastmod><changefreq>%s</changefreq><priority>%.1f</priority></url>', htmlspecialchars($url), $date, $changefreq, $priority
            );
            $this->io->streamWrite($xml);
        }
    }

}