<?php

/**
 * Magento sitemap custom urls
 *
 * @category   LCB
 * @package    LCB_Sitemap
 * @author     Silpion Tomasz Gregorczyk <tomasz@silpion.com.pl>
 */
$installer = $this;
$installer->startSetup();
$sql = <<<SQLTEXT
DROP TABLE IF EXISTS `{$this->getTable('lcb_sitemap_links')}`;
CREATE TABLE `{$this->getTable('lcb_sitemap_links')}` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `priority` smallint(6) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

SQLTEXT;

$installer->run($sql);
$installer->endSetup();
