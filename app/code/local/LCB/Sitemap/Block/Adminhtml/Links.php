<?php

/**
 * Magento sitemap custom urls
 *
 * @category   LCB
 * @package    LCB_Sitemap
 * @author     Silpion Tomasz Gregorczyk <tomasz@silpion.com.pl>
 */
class LCB_Sitemap_Block_Adminhtml_Links extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct()
    {

        $this->_controller = "adminhtml_links";
        $this->_blockGroup = "lcb_sitemap";
        $this->_headerText = Mage::helper("lcb_sitemap")->__("Links Manager");
        $this->_addButtonLabel = Mage::helper("lcb_sitemap")->__("Add New Item");
        parent::__construct();
    }

}
