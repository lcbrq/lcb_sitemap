<?php

/**
 * Magento sitemap custom urls
 *
 * @category   LCB
 * @package    LCB_Sitemap
 * @author     Silpion Tomasz Gregorczyk <tomasz@silpion.com.pl>
 */
class LCB_Sitemap_Block_Adminhtml_Links_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct()
    {
        parent::__construct();
        $this->setId("links_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("lcb_sitemap")->__("Item Information"));
    }

    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("lcb_sitemap")->__("Item Information"),
            "title" => Mage::helper("lcb_sitemap")->__("Item Information"),
            "content" => $this->getLayout()->createBlock("lcb_sitemap/adminhtml_links_edit_tab_form")->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

}
