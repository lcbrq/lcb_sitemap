<?php

/**
 * Magento sitemap custom urls
 *
 * @category   LCB
 * @package    LCB_Sitemap
 * @author     Silpion Tomasz Gregorczyk <tomasz@silpion.com.pl>
 */
class LCB_Sitemap_Block_Adminhtml_Links_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct()
    {

        parent::__construct();
        $this->_objectId = "id";
        $this->_blockGroup = "lcb_sitemap";
        $this->_controller = "adminhtml_links";
        $this->_updateButton("save", "label", Mage::helper("lcb_sitemap")->__("Save Item"));
        $this->_updateButton("delete", "label", Mage::helper("lcb_sitemap")->__("Delete Item"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("lcb_sitemap")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
                ), -100);

        $this->_formScripts[] = "function saveAndContinueEdit(){
				    editForm.submit($('edit_form').action+'back/edit/');
				  }";
    }

    public function getHeaderText()
    {
        if (Mage::registry("links_data") && Mage::registry("links_data")->getId()) {

            return Mage::helper("lcb_sitemap")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("links_data")->getId()));
        } else {

            return Mage::helper("lcb_sitemap")->__("Add Item");
        }
    }

}
