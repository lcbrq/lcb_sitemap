<?php

/**
 * Magento sitemap custom urls
 *
 * @category   LCB
 * @package    LCB_Sitemap
 * @author     Silpion Tomasz Gregorczyk <tomasz@silpion.com.pl>
 */
class LCB_Sitemap_Block_Adminhtml_Links_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("sitemap_form", array("legend" => Mage::helper("sitemap")->__("Item information")));

        $fieldset->addField("url", "text", array(
            "label" => Mage::helper("lcb_sitemap")->__("Url"),
            "name" => "url",
        ));

        if (Mage::getSingleton("adminhtml/session")->getLinksData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getLinksData());
            Mage::getSingleton("adminhtml/session")->setLinksData(null);
        } elseif (Mage::registry("links_data")) {
            $form->setValues(Mage::registry("links_data")->getData());
        }
        return parent::_prepareForm();
    }

}
