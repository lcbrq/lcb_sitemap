<?php

/**
 * Magento sitemap custom urls
 *
 * @category   LCB
 * @package    LCB_Sitemap
 * @author     Silpion Tomasz Gregorczyk <tomasz@silpion.com.pl>
 */
class LCB_Sitemap_Adminhtml_LinksController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("lcb_sitemap/links")->_addBreadcrumb(Mage::helper("adminhtml")->__("Links  Manager"), Mage::helper("adminhtml")->__("Links Manager"));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("Sitemap"));
        $this->_title($this->__("Manager Links"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__("Sitemap"));
        $this->_title($this->__("Links"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("lcb_sitemap/links")->load($id);
        if ($model->getId()) {
            Mage::register("links_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("lcb_sitemap/links");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Links Manager"), Mage::helper("adminhtml")->__("Links Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Links Description"), Mage::helper("adminhtml")->__("Links Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("lcb_sitemap/adminhtml_links_edit"))->_addLeft($this->getLayout()->createBlock("lcb_sitemap/adminhtml_links_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("lcb_sitemap")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction()
    {

        $this->_title($this->__("Sitemap"));
        $this->_title($this->__("Links"));
        $this->_title($this->__("New Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("lcb_sitemap/links")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("links_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("lcb_sitemap/links");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Links Manager"), Mage::helper("adminhtml")->__("Links Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Links Description"), Mage::helper("adminhtml")->__("Links Description"));


        $this->_addContent($this->getLayout()->createBlock("lcb_sitemap/adminhtml_links_edit"))->_addLeft($this->getLayout()->createBlock("lcb_sitemap/adminhtml_links_edit_tabs"));

        $this->renderLayout();
    }

    public function saveAction()
    {

        $post_data = $this->getRequest()->getPost();


        if ($post_data) {

            try {

                $model = Mage::getModel("lcb_sitemap/links")
                        ->addData($post_data)
                        ->setId($this->getRequest()->getParam("id"))
                        ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Links was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setLinksData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setLinksData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }
        }
        $this->_redirect("*/*/");
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("lcb_sitemap/links");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("lcb_sitemap/links");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName = 'links.csv';
        $grid = $this->getLayout()->createBlock('lcb_sitemap/adminhtml_links_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction()
    {
        $fileName = 'links.xml';
        $grid = $this->getLayout()->createBlock('lcb_sitemap/adminhtml_links_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

}
